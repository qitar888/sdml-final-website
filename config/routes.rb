Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root :to => "poems#index"
  resources :poems do
    member do
      post :clap
    end
  end

  mount ActionCable.server => '/cable'
end
