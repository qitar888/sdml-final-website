# coding: utf-8
module PoemsHelper
  def poem_layout(text = @poem.full,new_line: true)
    text.split("").each_slice(20).map do |slice|
      slice.insert(20, new_line ? "。<br/>" : "。")
      slice.insert(15, "，")
      slice.insert(10, new_line ? "；<br/>" : "；")
      slice.insert(5, "，")
      slice
    end.join
  end
end
