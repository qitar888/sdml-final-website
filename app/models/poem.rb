# coding: utf-8
class Poem < ApplicationRecord
  validates_length_of :short, is: 4, if: -> {genre == "絕句"}, message: "絕句請輸入四字"
  validates_length_of :short, is: 8, if: -> {genre == "律詩"}, message: "律詩請輸入八字"

  before_create :build_full
  belongs_to :author

  paginates_per 10

  @@url_base = "http://cml11.csie.ntu.edu.tw:8081/"

  def build_full
    @short_simplified = Ropencc.conv('t2s.json', short)
    response = infer_poem_with_heads_and_signal.gsub("\n","")
    self.full = Ropencc.conv('s2t.json', response)
  end

  def serialize
    { id: id, short: short, full: full, created_at: created_at, clap: clap }
  end

  private

  def infer_poem_with_heads_and_random_signal
    uri = URI(@@url_base + 'infer_poem_with_heads_and_random_signal')
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = false
    request = Net::HTTP::Post.new(uri.request_uri, {'Content-Type' =>'application/json'})

    request.body = JSON.generate({:heads => @short_simplified})

    response = http.request(request)
    JSON.parse(response.body)["outputs"]
  end

  def infer_poem_with_heads_and_signal
    uri = URI(@@url_base + 'infer_poem_with_heads_and_signal')
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = false
    request = Net::HTTP::Post.new(uri.request_uri, {'Content-Type' =>'application/json'})

    request.body = JSON.generate(
      { heads: @short_simplified || "測試資料",
        author: author&.simplified_name || "白居易",
        tones: (genre == "絕句") ? get_tones(20) : get_tones(40),
        "POS": (genre == "絕句") ? get_POS(20) : get_POS(40),
        rhymes: (genre == "絕句") ? get_rhymes(4) : get_rhymes(8),
      })

    response = http.request(request)
    JSON.parse(response.body)["outputs"]
  end

  def get_random_signals
    uri = URI.parse(@@url_base + "get_random_signals")
    http = Net::HTTP.new(uri.host, uri.port)
    result = http.get(uri.request_uri)
    JSON.parse result.body
  end

  ['tones', 'POS', 'rhymes'].each do |phrase|
    define_method("get_#{phrase}") do |length|
      loop do
        hash = get_random_signals
        if hash[phrase].split(" ").length == length
          return hash[phrase]
        end
      end
    end
  end
end
