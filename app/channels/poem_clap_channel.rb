class PoemClapChannel < ApplicationCable::Channel
  def subscribed
    stream_from "poem_clap_channel"
  end

  def unsubscribed
  end
end
