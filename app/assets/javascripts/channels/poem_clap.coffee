updatePoem = (poem) ->
  clapElements = document.querySelectorAll(".poem-#{poem.id}-clap")
  for clap in clapElements
    clap.textContent = poem.clap

updatePage = (poems_html) ->
  table = document.querySelector("#poem-rank")
  table.innerHTML = poems_html


App.poem_clap = App.cable.subscriptions.create "PoemClapChannel",

  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    updatePoem(data.poem)
    updatePage(data.poems)
