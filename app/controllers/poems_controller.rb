# coding: utf-8
class PoemsController < ApplicationController
  def index
    @poems = Poem.all.order(clap: :desc).page(params[:page])
    @new_poem = Poem.new
    @poem = Poem.find_by(id: params[:id].to_i)
  end

  def show
  end

  def create
    @poem = Poem.new(poem_params)

    if @poem.save
      redirect_to poems_path(id: @poem.id, page: Poem.count / 10 + 1)
    else
      flash[:error] = @poem.errors.map {|attr, error| error}.join("，")
      redirect_to poems_path
    end
  end

  def clap
    @poem = Poem.find(params[:id])
    @poem.clap += 1
    @poem.save
    page = Poem.order(clap: :desc).all.pluck(:id).index(@poem.id) / 10 + 1
    @poems = Poem.all.order(clap: :desc).page(page)
    poems_html = render(template: "poems/_poems_table.html.slim", locals: {poems: @poems}, layout: false)

    ActionCable.server.broadcast 'poem_clap_channel',
                                 {poem: {id: @poem.id, clap: @poem.clap},
                                  poems: poems_html }
  end

  private
  def poem_params
    params.require(:poem).permit(:id, :short, :genre, :author_id)
  end
end
