class AddAuthorIdToPoems < ActiveRecord::Migration[5.2]
  def change
    add_reference :poems, :author, index: true
  end
end
