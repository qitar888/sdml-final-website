class AddAmountToAuthor < ActiveRecord::Migration[5.2]
  def change
    add_column :authors, :amount, :integer
  end
end
