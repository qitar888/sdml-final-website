class AddSimplifiedNameToAuthor < ActiveRecord::Migration[5.2]
  def change
    add_column :authors, :simplified_name, :string
  end
end
