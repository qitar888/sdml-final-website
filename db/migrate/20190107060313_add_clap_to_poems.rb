class AddClapToPoems < ActiveRecord::Migration[5.2]
  def change
    add_column :poems, :clap, :integer, default: 0
  end
end
