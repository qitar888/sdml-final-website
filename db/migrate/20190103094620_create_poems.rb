class CreatePoems < ActiveRecord::Migration[5.2]
  def change
    create_table :poems do |t|
      t.string :short
      t.string :genre
      t.string :full
      t.string :author

      t.timestamps
    end
  end
end
