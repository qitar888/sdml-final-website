class RemoveAuthorFromPoem < ActiveRecord::Migration[5.2]
  def change
    remove_column :poems, :author, :string
  end
end
